The Contextual server variables module allows for server variables ($_SERVER) to
be used as Context module conditions.

An example usecase would be running a regular expression or basic check against
HTTP_HOST to determine if the site is in Development, Staging or Production
mode.

Contextual server variables was written and is maintained by Stuart Clark
(deciphered).
- http://stuar.tc/lark
- http://twitter.com/Decipher



Required modules
--------------------------------------------------------------------------------

* Context  - http://drupal.org/project/context
